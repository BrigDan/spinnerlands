#!/usr/bin/env python3
#This file is part of Spinnerlands.
#
#Spinnerlands is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Spinnerlands is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Spinnerlands.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import random
import string

#Arrays
symbol= ["Cherry", "Bell", "Seven", "Legs", "Marcus", "Psycho", "Vault", "Eridium"]
manufacturer = ["Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof", "Bandit", "Dahl", "Hyperion", "Jackobs", "Maliwan", "Tediore", "Tediore", "Tourge", "Vladof"]

#Jackpot Engine
random.shuffle(symbol)
roll = symbol[0]
print(roll)

#Gun Engine

gunbuilt = False
if gunbuilt == False:
    builtgun = [] # Index 0 is Barrel, 1 is Body, 2 is Stock, 4 is Grip, 5 is Sights
    partsdone = 0

    while partsdone < 5:
        random.shuffle(manufacturer)
        part = manufacturer[0]
        builtgun.append(part)
        partsdone = partsdone + 1
        gunbuilt = True

    print(builtgun)

#Size of the window
roomWidth, roomHeight = (1280, 720)
#Create the window
screen = pygame.display.set_mode((roomWidth, roomHeight))

#Update the screen
pygame.display.flip()

clock = pygame.time.Clock()

pygame.font.init()

gameFont=pygame.font.SysFont('Arial', 12)


def drawText(string,x,y,color):
	global screen
	global gameFont
	screen.blit(gameFont.render(string,True,color),(x,y))

while True:
	#Wait 1/60 seconds because we're running at 60 frames
	#per second
	clock.tick(60)
	#Just so we have something to draw for now
	screen.fill((0xff,0x9f,0xe6))

	#Events is a list that holds things that happened
	#since the last frame
	events = pygame.event.get()
	for event in events:
		#character input handling
		if event.type == pygame.KEYUP:
			print(chr(event.key))
			#Keycode is in event.key
			print(event.key)
		elif event.type == pygame.QUIT:
			pygame.font.quit()
			pygame.quit()
			exit()

	#Update screen
	pygame.display.flip()